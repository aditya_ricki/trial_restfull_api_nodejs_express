const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')

const app = express()

// import model
const db = require('./app/models')

// whitelist cors
let whiteList = [
	'http://localhost:8081',
]

// cors option
let corsOption = {
	origin: (origin, callback) => {
		if (whiteList.indexOf(origin) !== -1 || !origin) {
			callback(null, true)
		} else {
			callback(new Error('Not allowed by CORS'))
		}
	}
}

// use cors
app.use(cors(corsOption))

// sync db
db.sequelize.sync()

// use body-parser
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
	extended: true
}))

// article routes
require('./app/routes/article.routes.js')(app)

// app.get('/', (req, res) => {
// 	res.json({
// 		message: 'Welcome to restfull api'
// 	})
// })

// konfigurasi PORT
const PORT = process.env.PORT || 8080

// aktivasi server
app.listen(PORT, () => {
	console.log(`Server is running on http://localhost:${PORT}`)
})
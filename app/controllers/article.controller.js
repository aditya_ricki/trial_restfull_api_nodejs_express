const db = require('../models')
const Article = db.articles
const Option = db.Sequelize.Op

// create
exports.create = (req, res) => {
	// validate
	if (!req.body.title) {
		res.status(400).send({
			message: 'Content can\'t be null'
		})

		return
	}

	// create article
	const article = {
		title: req.body.title,
		content: req.body.content,
		image: req.body.image,
		published: req.body.published ? req.body.published : false
	}

	// query
	Article.create(article)
		.then((data) => {
			res.send(data)
		}).catch((err) => {
			res.status(500).send({
				message: err.message || 'Some error occured while creating the Article'
			})
		})
}

// retrieve all article
exports.findAll = (req, res) => {
	const title = req.query.title
	let condition = title ? {
		title: {
			[Op.like]: `%${title}%`
		}
	} : null

	// query
	Article.findAll({
		where: condition
	}).then((data) => {
		if (data.length < 1) {
			res.send({
				message: 'No articles have been submited'
			})
		} else {
			res.send(data)
		}
	}).catch((err) => {
		res.status(500).send({
			message: err.message || 'Some error occured while find article'
		})
	})
}

// find a single article with id
exports.findOne = (req, res) => {
	const id = req.params.id

	// query
	Article.findByPk(id)
		.then((data) => {
			if (data.length < 1) {
				res.send({
					message: `Articles with id ${id} were not found`
				})
			} else {
				res.send(data)
			}
		}).catch((err) => {
			res.status(500).send({
				message: `Error retrieving article with id ${id}`
			})
		})
}

// update article
exports.update = (req, res) => {
	const id = req.params.id

	Article.update(req.body, {
		where: {
			id: id
		}
	}).then((data) => {
		if (data == 1) {
			res.send({
				message: 'Article was updated successfully'
			})
		} else {
			res.send({
				message: `Can't update article with id ${id}`
			})
		}
	}).catch((err) => {
		res.status(500).send({
			message: `Error updating article with id ${id}`
		})
	})
}

// delete article
exports.delete = (req, res) => {
	const id = req.params.id

	Article.destroy({
		where: {
			id: id
		}
	}).then((data) => {
		if (data == 1) {
			res.send({
				message: 'Article was deleted successfully'
			})
		} else {
			res.send({
				message: `Can't delete article with id ${id}`
			})
		}
	}).catch((err) => {
		res.statuc(500).send({
			message: `Couldn't delete article with id ${id}`
		})
	})
}

// delete all post
exports.deleteAll = (req, res) => {
	Article.destroy({
		where: {},
		truncante: true
	}).then((data) => {
		message: `${data} article were delete successfully`
	}).catch((err) => {
		res.status(500).send({
			message: err.message || 'Some error ocured while removing all article'
		})
	})
}

// find all published articles
exports.findAllPublished = (req, res) => {
	Article.findAll({
		where: {
			published: true
		}
	}).then((data) => {
		res.send(data)
	}).catch((err) => {
		res.status(500).send({
			message: err.message || 'Some error occured retrieving article'
		})
	})
}
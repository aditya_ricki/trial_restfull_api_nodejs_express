module.exports = {
	HOST: 'localhost',
	USER: 'root',
	PASSWORD: '',
	DB: 'trial_error_restfulapi_mevn',
	dialect: 'mysql',
	pool: {
		max: 5,
		min: 0,
		acquire: 30000,
		idle: 10000
	}
}
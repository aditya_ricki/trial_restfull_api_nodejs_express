module.exports = (app) => {
	const articles = require('../controllers/article.controller.js')

	let router = require('express').Router()

	// route create
	router.post('/articles/', articles.create)

	// retrieve all article
	router.get('/articles/', articles.findAll)

	// retrieve single article
	router.get('/articles/:id', articles.findOne)

	// update article
	router.put('/articles/:id', articles.update)

	// delete post
	router.delete('/articles/:id', articles.delete)

	// delete all post
	router.delete('/articles/', articles.deleteAll)

	// find all published
	router.get('/published', articles.findAllPublished)

	// prefix
	app.use('/api', router)
}